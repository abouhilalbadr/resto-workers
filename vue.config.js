module.exports = {
    pwa: {
        name: 'UM6P restaurant',
        themeColor: '#0023b4',
        appleMobileWebAppCapable: 'yes',
        appleMobileWebAppStatusBarStyle: 'black-translucent'
    }
}