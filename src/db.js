import firebase from 'firebase/app';
import 'firebase/database';
import 'firebase/auth';

const config = {
    apiKey: "AIzaSyAtWrBrTsSMNXEZ4A3TEr3yIqBnsNLZFSU",
    authDomain: "visualisation-restaurant.firebaseapp.com",
    databaseURL: "https://visualisation-restaurant.firebaseio.com",
    projectId: "visualisation-restaurant",
    storageBucket: "visualisation-restaurant.appspot.com",
    messagingSenderId: "42970900801",
    appId: "1:42970900801:web:1f5d196c4e07cb5b5259c0"
};
firebase.initializeApp(config);

export default firebase;
